% SSRC paper class
%
% $Log: ssrcpaper.cls,v $
% Revision 1.1  2008/11/09 20:08:20  aleung
% Adding the initial template for a HotOS paper on metadata management
%
% Revision 1.1  2008/07/14 04:56:02  aleung
% Adding initial files
%
% Revision 1.1  2007/11/19 18:35:42  aleung
% adding the seed of the osdi repo
%
% Revision 1.1  2007/10/08 18:19:49  aleung
% adding the foundation of the usenix paper
%
% Revision 1.1  2007/06/24 03:00:49  aleung
% setting up the initial directory for fast cifs analysis paper
%
% Revision 1.1  2007/05/16 18:39:42  aleung
% added the initial files for the cifs analysis paper
%
% Revision 1.2  2007/04/04 17:39:30  elm
%
% subfigures used now, along with wrapfig.  Minor abstract change.
%
% Revision 1.1.1.1  2006/07/23 18:43:36  aleung
% Starting a new directory for a USENIX OR USENIX Security paper
%
% Revision 1.1  2006/07/12 03:55:19  aleung
% added the .cls file
%
% Revision 1.15  2003/05/11 18:04:19  elm
% Changed size of subcaption figures.
%
% Revision 1.14  2002/12/13 21:05:36  elm
% Small change to SSRC tech report title page.
%
% Revision 1.11  2002/08/29 20:19:30  elm
% Changed "draft" option to "draftlabel".
%
% Revision 1.10  2002/08/29 19:32:33  elm
% Fixed the draft option.
%
% Revision 1.9  2002/08/29 19:24:30  elm
% Added a "draft" option that prints the word draft along the bottom.
%
% Revision 1.8  2002/07/31 20:18:12  elm
%
%
% More modifications for IEEE papers.
%
% Revision 1.7  2002/07/29 22:11:20  elm
% Modified ssrcpaper.cls to take the ieee argument and use the latex8
% style when it does so.
%
% Revision 1.6  2002/07/29 21:52:32  elm
% Added options for IEEE formatting.
%
% Revision 1.5  2002/06/09 22:51:11  elm
% Added a nocomments option.  Specifying this option turns off all
% comments.  You may wish to use this option when printing copies
% for submission.
%
% Revision 1.4  2002/06/08 00:18:29  elm
% Added note environment and shortnote command for inserting notes into
% the paper.  Each takes an argument that specifies who's making the
% note.  For example, \shortnote{elm}{Here's my note.}.
%
% Notes can be made to disappear by including the "final" argument to
% the ssrcpaper class in \documentclass.
%
% Revision 1.3  2002/06/07 23:16:03  elm
% Fixed it so that an empty \publishedin doesn't produce a small (empty)
% box.
%
% Revision 1.2  2002/06/07 23:01:59  elm
% Made the techreport option more robust.  We now have a real title page,
% along with a command to specify tech report number (\trnumber).
%
% Revision 1.1  2002/06/07 21:57:53  elm
% Added a template file for SSRC papers.  For now, this makes it easier
% to add "published in" footers and specify single or double spacing
% (using the singlespace or doublespace option).  More features to
% come....
%
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ssrcpaper}

\RequirePackage{ifthen}

\newcommand{\ssrc@ls}{1}
\newboolean{ssrc@ieee}
\setboolean{ssrc@ieee}{false}
\newboolean{ssrc@tr}
\setboolean{ssrc@tr}{false}
\newboolean{ssrc@final}
\setboolean{ssrc@final}{false}
\newboolean{ssrc@comments}
\setboolean{ssrc@comments}{true}
\newboolean{ssrc@draft}
\setboolean{ssrc@draft}{false}

\DeclareOption{doublespace}{\renewcommand{\ssrc@ls}{1.6}}
\DeclareOption{singlespace}{\renewcommand{\ssrc@ls}{1}}

\DeclareOption{draftlabel}{
  \setboolean{ssrc@draft}{true}
}

\DeclareOption{ieee}{
  \setboolean{ssrc@ieee}{true}
}

\DeclareOption{techreport}{
  \setboolean{ssrc@tr}{true}
  \PassOptionsToClass{titlepage}{article}
}

\DeclareOption{final}{
  \setboolean{ssrc@final}{true}
  \setboolean{ssrc@comments}{false}
  \PassOptionsToClass{final}{article}
}

\DeclareOption{nocomments}{
  \setboolean{ssrc@comments}{false}
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions

\LoadClass{article}
\RequirePackage{times}
\RequirePackage{mathptmx}
\RequirePackage{fullpage}
\RequirePackage{graphicx}
\RequirePackage{subfigure}
\RequirePackage{fancyhdr}
\RequirePackage{verbatim}
\ifthenelse{\boolean{ssrc@draft}}
  {\RequirePackage[none,bottom]{draftcopy}}
  {}

% Allow more floats than the default.
\renewcommand\floatpagefraction{.9}
\renewcommand\dblfloatpagefraction{.9}
\renewcommand\topfraction{.9}
\renewcommand\dbltopfraction{.9}
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1}
\setcounter{totalnumber}{10}
\setcounter{topnumber}{10}
\setcounter{dbltopnumber}{10}
\setcounter{bottomnumber}{10}

% Set values for float separation from text
\setlength{\floatsep}{1.5ex plus1.0ex minus 0.2ex}
\setlength{\dblfloatsep}{1.5ex plus1.0ex minus 0.2ex}
\setlength{\textfloatsep}{1.5ex plus1.0ex minus 0.2ex}
\setlength{\dbltextfloatsep}{1.5ex plus1.0ex minus 0.2ex}
\setlength{\abovecaptionskip}{0.5ex}
\setlength{\belowcaptionskip}{0.5ex}

% Adjust values for subfigures
\renewcommand*{\subfigtopskip}{0.5ex}
\renewcommand*{\subfigbottomskip}{0.5ex}
\renewcommand*{\subfigcapskip}{0.5ex}
\renewcommand*{\subfigcapmargin}{0.5ex}
\renewcommand*{\subcapsize}{\small}

% Don't allow widows
\widowpenalty=10000

% Use the \publishedin command to set the information about where the
% paper was published
\newcommand{\ssrc@publishedin}{}
\newcommand{\publishedin}[1]{\renewcommand{\ssrc@publishedin}{#1}}

% Use this command to set up the tech report number
\newcommand{\ssrc@trnum}{}
\newcommand{\trnumber}[1]{\renewcommand{\ssrc@trnum}{#1}}

% Use this command to set up the tech report date
\newcommand{\ssrc@trdate}{\today}
\newcommand{\trdate}[1]{\renewcommand{\ssrc@trdate}{#1}}
% This sets up the width of the ``published in'' box.
\newlength{\ssrc@pubwidth}
\setlength{\ssrc@pubwidth}{\textwidth}
\addtolength{\ssrc@pubwidth}{-20pt}

% This command lets you easily document where the paper was published.
% The information is boxed at the bottom of the page on which the
% \publishedin command is used.
\newcommand{\ssrc@publishedbox}{
  \newcommand*{\ps@publishedbox}{%
    \renewcommand*{\@oddhead}{}
    \renewcommand*{\@evenhead}{\@oddhead}
    \renewcommand*{\@oddfoot}{
      \ifthenelse{\equal{\ssrc@publishedin}{} \or \boolean{ssrc@final} \or
        \boolean{ssrc@tr}}
      {
        % No footer if there's no publishedin or if it's the final version
        % or if it's a tech report
      }{
        \centering\protect\fbox{\parbox{\ssrc@pubwidth}{\small\ssrc@publishedin}}
      }
    }
    \renewcommand*{\@evenfoot}{\@oddfoot}
  }
  \thispagestyle{publishedbox}
}

% Redefine the maketitle command
\let\ssrc@maketitle\maketitle
\renewcommand{\maketitle}{
  \ifthenelse{\boolean{ssrc@tr}}
  {
    \begin{titlepage}
    \begin{center}
      \rule{2in}{0in} \par
      \vspace{2in}
      {\Huge\@title} \par
      \vspace{1em}
      {\huge Technical Report \ssrc@trnum} \par
      \vspace{2em}
      {\Large
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}
      } \par
      \vspace{2em}
      {\Large
        Storage Systems Research Center \\
        Jack Baskin School of Engineering \\
        University of California, Santa Cruz \\
        Santa Cruz, CA 95064 \\
        \texttt{http://ssrc.cse.ucsc.edu/} \\
        \vspace{12pt}
        \ssrc@trdate
      }
      \vspace{1in}
%      {\large
%       \begin{minipage}{5in}
%       \ssrc@publishedin
%       \end{minipage}
%      }
    \end{center}
    \end{titlepage}
    \newpage
  }
  {
    \ssrc@maketitle
    \ssrc@publishedbox
  }
}

\newsavebox{\commentsbox}
\newsavebox{\commentsname}

\newenvironment{note}[1]
{
 \sbox{\commentsname}{\textbf{#1}}
 \begin{lrbox}{\commentsbox}
   \begin{minipage}{\columnwidth}
   \bfseries \noindent COMMENT~(\usebox{\commentsname}) \par
   \begin{quote}
}
{
  \end{quote}
  \end{minipage}
 \end{lrbox}
 \ifthenelse{\boolean{ssrc@comments}}
 {\usebox{\commentsbox}}
 {}
}

\newcommand{\shortnote}[2]{\ifthenelse{\boolean{ssrc@comments}}{\textbf{\large #1:
#2}}{}}

% Set the default line spacing
\renewcommand{\baselinestretch}{\ssrc@ls}

% Set up page size & sectioning commands for IEEE
\ifthenelse{\boolean{ssrc@ieee}}
  {\RequirePackage{latex8}}
  {}

% If this is the final version, omit the page numbers
\ifthenelse{\boolean{ssrc@final}}
  {\pagestyle{empty}}
  {\pagestyle{plain}}
