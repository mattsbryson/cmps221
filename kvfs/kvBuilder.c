#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include "dictionary.h"
#include "kvBuilder.h"

DICTIONARY* dirBuilder(char* name)
{
	FILE* in = fopen(name, "r");
	if( in==NULL )
	{
		printf("Unable to read from file \n");
		return(NULL);
	}
	
	DICTIONARY* dirDict = InitDictionary();
	
	int lineSize = 256;
	char line[lineSize];
	
	while( fgets(line, lineSize, in) != NULL )
	{
		char* name = malloc(256*sizeof(char));
		char* guid = malloc(256*sizeof(char));
		
		sscanf(line, "%s %s", name, guid);
		
		DAdd(dirDict, guid, name);
		
	}
	
	fclose(in);
	return dirDict;
}