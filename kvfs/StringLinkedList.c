//-----------------------------------------------------------------------------
//   IntegerLinkedList.c
//   Implementation file for IntegerLinkedList ADT
//-----------------------------------------------------------------------------
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include"StringLinkedList.h"

// private types --------------------------------------------------------------

// Node
//see .h file for defn 

// newNode()
// constructor of the Node type
Node newNode(char* s) {
   Node N = malloc(sizeof(NodeObj));
   assert(N!=NULL);
   N->item = s;
   N->next = NULL;
   return(N);
}

// freeNode()
// destructor for the Node type
void freeNode(Node *pN){
   if( pN!=NULL && *pN!=NULL){
      free(*pN);
      *pN = NULL;
   }
}

// public functions -----------------------------------------------------------

// newLinkedList()
// constructor for the LinkedList type
LinkedList newLinkedList(void){
   LinkedList S = malloc(sizeof(LinkedListObj));
   assert(S!=NULL);
   S->top = NULL;
   S->numItems = 0;
   return S;
}

// freeLinkedList()
// destructor for the LinkedList type
void freeLinkedList(LinkedList* pS){
   if( pS!=NULL && *pS!=NULL)
   {
	  Node N = (*pS)->top; 
	  Node temp;
      while(N != NULL) 
	  {
		temp = N->next;
	  	freeNode(&N);
		N = temp; 
	  }
	  freeNode(&N);
	  free(*pS);
      *pS = NULL;
   }
}

//-----------------------------------------------------------------------------
// definitions of ADT operations (only giving printLinkedList, you need to do the rest)
//-----------------------------------------------------------------------------

Node find(char* s, LinkedList S)
{
	Node N;
	for(N=S->top; N!=NULL; N=N->next) 
	{
		if(strcmp(N->item,s) == 1)
		{
			return N;
		}
	}
	return NULL;
}

void addNode(Node N, LinkedList S)
{
	Node currentNode;
	if(S->top == NULL)
	{
		S->top = N;
	}
	else
	{
		currentNode = S->top; 
		while(currentNode->next != NULL)
		{
			currentNode = currentNode->next; 
		}
		currentNode->next = N; 
	}
	S->numItems++;
}

void insert(char* s, LinkedList S)
{
	Node N = newNode(s);
	Node currentNode;
	if(S->top == NULL)
	{
		S->top = N;
	}
	else
	{
		currentNode = S->top; 
		while(currentNode->next != NULL)
		{
			currentNode = currentNode->next; 
		}
		currentNode->next = N; 
	}
	S->numItems++;
}

void printList(LinkedList S)
{
	Node temp = S->top;
	
	while(temp != NULL)
	{
		printf("%s,", temp->item);
		temp = temp->next;
	}
	printf("\n");
}

void delete(char* s, LinkedList S)
{
	Node N;
	Node prev;
	for(N=S->top; N!=NULL; N=N->next) 
	{
		if(N->item == s)
		{
			if(N == S->top)
			{
				S->top = N->next;
			}
			else
			{
				prev->next = N->next;
			}
			freeNode(&N);
			break;
		}
		prev = N;
	}
}

// printLinkedList()
// prints a text representation of S to the file pointed to by out
// pre: none
void printLinkedList(FILE* out, LinkedList S){
   Node N;
   if( S==NULL ){
      fprintf(stderr, 
              "LinkedList Error: calling printLinkedList() on NULL LinkedList reference\n");
      exit(EXIT_FAILURE);
   }
   for(N=S->top; N!=NULL; N=N->next)
   {
	   if(N->next != NULL)
	   {
	   		fprintf(out, "%s -> ", N->item);
	   }
	   else
	   {
	   		fprintf(out, "%s", N->item);
	   }
   }
   fprintf(out, "\n");
}
