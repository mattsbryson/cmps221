#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include "dictionary.h"
#include "kvBuilder.h"
#include "StringLinkedList.h"

int main(int argc, char* argv[])
{
	
	if(argc < 2)
	{
        printf("Usage: %s <input file>\n", argv[0]);
        exit(EXIT_FAILURE);
	}
	
	/*
	DICTIONARY* myDict = dirBuilder(argv[1]);
	
	LinkedList keys = keyList(myDict);
	
	printList(keys);
	
	*/
	
	char* hardDir = "fakeDir";
	DICTIONARY* dir = dirBuilder(hardDir);
	
	LinkedList dirList = keyList(dir);
	
	Node temp = dirList->top;
	
	while(temp != NULL)
	{
		//printf("%s,", temp->item);
		//filler( buffer, temp->item, NULL, 0 ); 
		printf("Wrote %s to buffer: ", temp->item);
		temp = temp->next;
	}
		
	freeLinkedList(&dirList);
	CleanDictionary(dir);
	free(dir);
	
   
   	return EXIT_SUCCESS;
}

//something something hash
unsigned long hash1(char* str) {
	unsigned long hash = 0;
  return hash;
}

/*
DICTIONARY* myDict = InitDictionary();

char* data1 = "I'm data!";
char* key = "key";

char* data2 = "i might be data";
char* k2 = "k2";

DAdd(myDict, data2, k2);
DAdd(myDict, data1, key);

char* gotData = GetDataWithKey(myDict, key);
char* moreData = GetDataWithKey(myDict, k2);

printf("%s\n %s\n", gotData, moreData);
*/