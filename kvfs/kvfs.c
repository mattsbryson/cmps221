/**
 * Simple & Stupid Filesystem.
 * 
 * Mohammed Q. Hussain - http://www.maastaar.net
 *
 * This is an example of using FUSE to build a simple filesystem. It is a part of a tutorial in MQH Blog with the title "Writing a Simple Filesystem Using FUSE in C": http://www.maastaar.net/fuse/linux/filesystem/c/2016/05/21/writing-a-simple-filesystem-using-fuse/
 *
 * License: GNU GPL
 */


/*
 * THIS IS A REALLY GREAT TUTORIAL:
 *
 * https://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/
 *
 */

#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "dictionary.h"
#include "StringLinkedList.h"
#include "kvBuilder.h"

static int do_getattr( const char *path, struct stat *st )
{
	printf( "[getattr] Called\n" );
	printf( "\tAttributes of %s requested\n", path );
	
	// GNU's definitions of the attributes (http://www.gnu.org/software/libc/manual/html_node/Attribute-Meanings.html):
	// 		st_uid: 	The user ID of the file’s owner.
	//		st_gid: 	The group ID of the file.
	//		st_atime: 	This is the last access time for the file.
	//		st_mtime: 	This is the time of the last modification to the contents of the file.
	//		st_mode: 	Specifies the mode of the file. This includes file type information (see Testing File Type) and the file permission bits (see Permission Bits).
	//		st_nlink: 	The number of hard links to the file. This count keeps track of how many directories have entries for this file. If the count is ever decremented to zero, then the file itself is discarded as soon 
	//						as no process still holds it open. Symbolic links are not counted in the total.
	//		st_size:	This specifies the size of a regular file in bytes. For files that are really devices this field isn’t usually meaningful. For symbolic links this specifies the length of the file name the link refers to.
	
	st->st_uid = getuid(); // The owner of the file/directory is the user who mounted the filesystem
	st->st_gid = getgid(); // The group of the file/directory is the same as the group of the user who mounted the filesystem
	st->st_atime = time( NULL ); // The last "a"ccess of the file/directory is right now
	st->st_mtime = time( NULL ); // The last "m"odification of the file/directory is right now
	
	if ( strcmp( path, "/" ) == 0 )
	{
		st->st_mode = S_IFDIR | 0755;
		st->st_nlink = 2; // Why "two" hardlinks instead of "one"? The answer is here: http://unix.stackexchange.com/a/101536
	}
	else
	{
		st->st_mode = S_IFREG | 0644;
		st->st_nlink = 1;
		st->st_size = 1024;
	}
	
	return 0;
}

static int do_readdir( const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi )
{
	//TODO fix path??
	//build kv store of directory 
	printf("path: %s", path);
	
	char* hardDir = "/Users/Matt/git/cmps221/kvfs/fakeDir";
	//DICTIONARY* dir = dirBuilder(hardDir);
	DICTIONARY* dir = InitDictionary();
	DAdd(dir, "guid1", "name6");
	DAdd(dir, "guid2", "name5");
	DAdd(dir, "guid3", "name4");
	
	LinkedList dirList = keyList(dir);
	
	Node temp = dirList->top;
	
	while(temp != NULL)
	{
		//printf("%s,", temp->item);
		filler( buffer, temp->item, NULL, 0 ); 
		printf("Wrote %s to buffer: ", temp->item);
		temp = temp->next;
	}
	
	
	filler( buffer, "fakeFile", NULL, 0 );
	
	
	//printf( "--> Getting The List of Files of %s\n", path );
	filler( buffer, ".", NULL, 0 ); // Current Directory
	filler( buffer, "..", NULL, 0 ); // Parent Directory
	
	if ( strcmp( path, "/" ) == 0 ) // If the user is trying to show the files/directories of the root directory show the following
	{
		filler( buffer, "file54", NULL, 0 );
		filler( buffer, "file349", NULL, 0 );
	}
	
	freeLinkedList(&dirList);
	CleanDictionary(dir);
	free(dir);
	return 0;
}

static int do_read( const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi )
{
	printf( "--> Trying to read %s, %zd, %zd\n", path, offset, size );
	
	char file54Text[] = "Hello World From File54!\n";
	char file349Text[] = "Hello World From File349!\n";
	char *selectedText = NULL;
	
	// ... //
	
	if ( strcmp( path, "/file54" ) == 0 )
		selectedText = file54Text;
	else if ( strcmp( path, "/file349" ) == 0 )
		selectedText = file349Text;
	//else
		//return -1;
	
	// ... //


  char filecontents[128];
  char tempstr[256];
  if (fi != NULL && fi->fh != -1) {
    read(fi->fh, filecontents, 127);
    sprintf(tempstr, "file %s contains:\n%s\n", path, filecontents);
  } else {
    sprintf(tempstr, "cannot read file\n");
    return -1;
  }

  selectedText = tempstr;
	memcpy( buffer, selectedText + offset, size );
		
	return strlen(selectedText) - offset; // return requested size unless IO_mapped (as per documentation)
}

// don't get a bunch of errors with swap files anymore but still no permissions
static int do_write( const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  int numchars = 0;
  if (fi != NULL) {
    numchars = write(fi->fh, buf + offset, size);
  }
  return numchars;
}

/*TODO: Should we be using syscall for open, read, write, and close?
 *TODO: Implement the following:

 File open operation

No creation (O_CREAT, O_EXCL) and by default also no truncation (O_TRUNC) flags will be passed to open().
If an application specifies O_TRUNC, fuse first calls truncate() and then open().
Only if 'atomic_o_trunc' has been specified and kernel version is 2.6.24 or later, O_TRUNC is passed on to open.

Unless the 'default_permissions' mount option is given, open should check
if the operation is permitted for the given flags. Optionally open may also
return an arbitrary filehandle in the fuse_file_info structure, which will be passed to all file operations.
 *
 */
static int do_open( const char *path, struct fuse_file_info *fi)
{
  if (fi != NULL) {
    fi->fh = open(path, fi->flags & ~O_TRUNC & ~O_CREAT); // there are separate
                                                          // funcs for these
                                                          // (trunc and create)
    return fi->fh;
  }
  return -1;
}

static int do_create( const char *path, mode_t mode, struct fuse_file_info *fi)
{
  if (fi != NULL) {
    fi->fh = open(path, fi->flags & ~O_TRUNC, 0x1ff);
    return 0;
  }
  return -1;
}

static int do_release( const char *path, struct fuse_file_info *fi)
{
  if (fi != NULL && fi->fh != -1) {
    return close(fi->fh);
  }
  return -1;
}

// can't figure out why these are giving me implicit declaration
static int do_utimens(const char *path, const struct timespec *tv)
{
  if (1) return 0; // allow so you can just call touch to create files
  FILE *fp = fopen(path, "a");
  futimens(fileno(fp), tv);
}

static struct fuse_operations operations = {
    .getattr	= do_getattr,
    .readdir	= do_readdir,
    .read		= do_read,
    .write  = do_write,
    .open   = do_open,
    //.create = do_create,
    .release = do_release,
    .utimens = do_utimens,
};

//weirdo hash function
unsigned long hash1(char* str) {
	unsigned long hash = 0;
  return hash;
}

int main( int argc, char *argv[] )
{
	return fuse_main( argc, argv, &operations, NULL );
}