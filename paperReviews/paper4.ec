<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3094609::1551814"
        submission="4"
        title="Mach: A New Kernel Foundation for Unix Development"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary & rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary & rating
    == below. This field is required.
    -->
	The Mach paper introduces a lot of functionality that we have in Unix today. The purpose of the project is almost to "reboot" UNIX, which the authors felt was moving away from its simplistic roots to become more complicated and less abstracted than the original design (because of the need for additional communication methods and functionality). Due to the number of contributions in this paper, I'll outline some of the main contributions. Copy on write was introduced, and has proved to be the best method for "copying" data when forking (still used today). Memory sharing was formally introduced so that external libraries were not required to provide memory sharing like features. The design as a whole tries to push many features out of the kernel. Task and threads are introduced to avoid the high overhead of processes, with all threads existing inside of tasks. This advance allowed for parallelism on shared memory. Tasks have a hierarchal memory format, with permisions using inheritance. Permissions are used on memory, and can only be reduced in this system. Communication is implemented in several different ways, the main being ports, which can be used as local message buffers, or used with a server to allow for network process communication. The Mach paper brings in a lot of the system abstraction used in UNIX today, which means that it was successful in its goal of simplifying UNIX (at the time). 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- Tasks & threads reduced system overhead. Was this the first time they were introduced? Has anything changed in systems today? 
	- Hierarchal memory management/mapping seems like a great idea. Is it still used today? 
	- Ports and message movement definitions were very well conceived. Have any significant changes been made (I can't think of any)?
  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Overall, the paper is well presented, though I feel that sections could be reorganized a bit to allow the paper to flow better. It is information dense, but that isn't a fault, it's part of what makes it a good paper, though it does slow reading a little. 



  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    4
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Mach is a core paper on the UNIX roadmap, and introduces many features used today (core concepts in OS). It should not be left out of any graduate OS class.

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>