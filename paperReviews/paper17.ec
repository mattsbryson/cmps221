<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3141272::1551814"
        submission="17"
        title="Mondrix: memory isolation for linux using mondriaan memory protection"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	Mondrix, a linux implementation of Mondriaan Memory Protection, seeks to provide word level (fine grain) control of memory resources on a lower than kernel level basis. Their goals are to increase security and reduce failure on machines without decreasing performance significantly. They do this by implementing a low level memory manager that gives out protection domains. These protection domains can be nested, and can be called across using entry points. In some ways they are very similar to protection domains described in other implementations that break memory allocation from memory protection, such as Opal. To accomplish this efficiently, they needed more hardware support, so they system is built on an emulator. They needed a gate look-aside buffer (GLB) for managing thread switches into other protection domains. Threads, when switching, can no longer write to areas of the stack that were a part of other protection domains. I wonder if this could result in execution issues for certain programs, or allow stacks to store privilege information with other protection domains. The paper presents their implementation of Linux on top of their memory management system, and explains how standard system tasks occur with it. Modules and drivers are loaded into their own protection domains inside the single kernel address space. The system also has the feature of allowing for group protection domains, so that multiple protection domains can access the same region of memory. Performance is evaluated as well, both with security and execution time. The system does a good job of catching errors and malicious attacks that otherwise might be missed - I found the one where linux frees the kernel stack to be interesting/amusing. Additionally, many disk exploits are caught, but 2 are missed and 3 are allowed to continue executing. However, they do say a journaled disk could resolve these issues, so I wonder how useful Mondrix is in that regard. Performance in time is slow, causing between 8-15 percent overhead. I believe this system could be useful in some instances requiring more extreme security, but on standard systems the overhead would not be worth the extra protection. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- Could the stack r/w system be exploited to allow for privileged information to be shared with other protection domains? 
	- Why not just implement this in the kernel and have modules exist outside of the kernel but not in userspace? (Protection rings or something like that)
	- What do you think the two instances that made it past their testing were?


  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Good presentation quality, good paper layout.





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    5
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, it shows that there doesn't need to be hardware support for you to suggest hardware change and that software change has the potential to potentially drive hardware change. Also, it is a novel approach to fine grain memory access, different from approaches we've read about before. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    3
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>