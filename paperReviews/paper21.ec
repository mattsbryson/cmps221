<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3170192::1551814"
        submission="21"
        title="An Empirical Study of Operating Systems Errors"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	"An Empirical Study of Operating Systems Errors" presents an intertesting and novel view of bug analysis in operating systems, using advanced statistics techniques to allow for conclusions to be made with high certainty from the errors detected in the linux (and open BSD) kernel over time. Error detection is done in two ways, resulting in a much more complete view of errors in the operating system than just simple log analysis. Log analysis is done automatically through the manual identification of many errors that are then used to allow the authors to automatically find these bugs throughout the OS source code (many versioned open sourced code is key for this project to have existed). In addition, several compiler extensions are used to detect issues, such as block, null, and var. They use these tools to build a database which is analyzed automatically, allowing for them to analyze bugs across many years of version changes in the linux kernel. They use statistical modeling to find and present many useful facts about the bug data gathered, and in each case explain the process of finding the distribution curve. They show the changes in bugs over time - there are more; the location of bugs - most exist in drivers; the clustering of bugs - bugs are certainly clustered due to programmer ignorance; and bugs life span, which turns out to be 1.8 years on average. All of these facts are well graphed, which gives more information about the distribution to the reader than the fact itself. The authors also compare against OpenBSD to give an alternate example to further their case. This paper uses advanced bug finding techniques in conjecture with advanced statistics to show new traits (or "prove") about bugs in operating systems. These are significant contributions and this is an excellent paper.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- What is a note? The paper explained it as "the number of times a check is applied" - so does that simply mean the number of times a type of bug occurred?
	- Why are relative errors calculated by errors/notes?
	- It's very interesting that they affected the number of bugs detected in the operating system.
 

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Excellent quality, especially given that this paper includes a lot of statistic methods that many readers may be unfamiliar with.





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    5
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes, it is a very interesting subset of papers that belong in operating systems, and shows the breadth of OS research. Additionally, it is a very interesting and novel paper.




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>