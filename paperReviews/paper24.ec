<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3188218::1551814"
        submission="24"
        title="Ironclad Apps: End-to-End Security via Automated Full-System Verification"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	Ironclad describes the creation of an automatically verified operating system, compiler, and apps that use the aforementioned operating systems. It sets out with the goals of providing security guarantees through proven code. This is accomplished by using Dafny, which uses the Z3 solver to prove code (given the necessary preconditions, postconditions, and loop invariants). Because Dafny is designed by microsoft to create .net code for microsoft OSes, the authors wrote DafnyCC, a dafny compiler that results in BoogieX86, which is verifiable assembly code. Because they also use verification techniques on the assembly code, all code does not need to come from Dafny. The assembly is then turned in to executable binary using a trusted compiler. I have an issue with this, since trusting a compiler means that it is correct, something that they don't seek to prove in this paper. Additionally, they prove some of the OS components, such as the memory allocated, which fits with the Dafny model and does garbage collection. Memory is guaranteed private as well. Several applications that would require correctness and privacy were implemented, such as a database that had differential privacy and a notary program. There were several issues in this paper, including proving the preconditions of functions calling functions, but were able to write a relatively simple precondition to prove this interaction correct, but that requires manual intervention. Additionally, function bodys are often ignored when proving correctness, which should be fine because of the existence of post conditions. They showed that correct code could be done within a reasonable amount of developer time (which is probably acceptable in the cases this would be used in), and that performance doesn't suffer too much (too orders of magnitude, before optimization).

A lot was introduced in this paper that would be useful for an organization needing a secure and correct application, and though I have some concerns regarding their trust of the assembly compiler/linker, and with the ability to automatically prove specs, I believe the work presents enough to overcome these issues (though I am very concerned with the ability to introduce bugs in specs).

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
   1 
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- What is nonlinear arithmetic? 
	- (Issues of Trust/Correctness Assumptions Below)
	- How can you trust a compiler and prove it correct?
	- How can you have a proven OS/application if there can be bugs in specs?
	- What if there is an error in Dafny or Boogie? (i.e. How can the OS be verified if they are not verified?)

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->

	Could be improved, as several foreign concepts are introduced before they are explained. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, it shows that provable correctness can be achieved for something as complicated as an OS. However, many details are glossed over and I believe some issues to exist. 
	
  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    3
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>