<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3132935::1551814"
        submission="14"
        title="Resource Containers: A New Facility for Resource Management in Server Systems"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	In "Resource Containers: A new facility for resource management in server systems," the authors argue that with the rise of the internet age and the pervasiveness of web servers, traditional scheduling techniques and the underlying "resource principal" is no longer sufficient. They believe that the assumption in modern operating systems that a resource principal is equivalent to a security context to be incorrect, and present a number of applications where this is true (mostly web servers). One example is a web server that uses a multi-threaded connection model with CGI to generate non static content. This would either be scheduled as two processes, instead of each task being scheduled separately (or as threads, which would be less than a task, in the event CGI is used). Additionally, they complain that even simple applications that use networking are not scheduled properly since schedulers don't account or charge for kernel use time. Despite harping on this point consistently, they don't seem to do better than the standard method for accounting for kernel network handling, which is Lazy Receiver Protocol (LRP). LRP attempts to attach all network traffic to a process, and charge it for the processing of the traffic. Other than wanting fair accounting of resources, the authors seem to have the goal of lessening the affects of heavy load on a server and allowing it to survive DDOS attacks. In their evaluation, it does survive DDOS attacks, but only if the server can identify bad connections, which means they did not provide an adequate solution. If you were to argue they could just look for heavily used connections and schedule them later than higher priority connections, you could be degrading the QoS of an actual user, and not an attacker. This same argument holds for their other metrics, as most revolve around giving one task a lower priority. Containers are a good idea for fair accounting of usage, but I would argue that they likely contribute little to performance and would be better as a construct of a exokernel (ie., the problem they are trying to solve could be better solved there). Additionally, the work isn't particularly novel, as the authors essentially admit with their related work section.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    -1
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- What did they mean by the processing time of a software interrupt being charged to the application being ran at the time? I did not think that was the case. 
	- They complain about LRP a lot, and then implement it. Is there a better solution than LRP?
	- Was any of this ever used in practice? Are CPU limitations ever imposed using containers?
	- Why not just use a VM?


  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	The presentation is repetitive and drawn out. The paper takes far too long to make far too simple of a point, and does not validate it along the way.



  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    2
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Probably not, I don't believe much of use is presented in this paper, other than management of a resource sections by tying them together in the same resource container. 
	
  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    2
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>