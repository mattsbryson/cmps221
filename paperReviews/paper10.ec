<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3118589::1551814"
        submission="10"
        title="Xen and the Art of Virtualization"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
Xen presents their take on para-virtualization, which is the first that was done on commodity hardware with commodity operating systems (linux, XP). They present this approach because systems that used virtualization incurred high overhead and were missing security features (one OS could ruin performance for the rest). Their solution requires changes to the OS "guest" (it seems they invented the terminology to speak about VMs that we use today mostly) to allow it to interact better with the hypervisor and be aware of its state as a VM. Xen is able to achieve incredible performance, coming very close to bare metal linux in most cases. It is able to achieve this because the operating system is put it charge of it's own memory, TLB, and disk access, with the hypervisor primarily acting as a security mechanism. I also expect the use of batching in all hardware use (CPU, mem, disk, network, etc.) to have helped performance significantly, as a hypercall (call to the hypervisor) isn't used for independent requests (less context switches to the hypervisor). I also found it interesting that Xen took advantage of x86's ability to run tasks at different hardware privilege levels, and was able to put the hypervisor, OS, and applications at their own levels. Also interesting was the memory balloon concept so that memory could be used as effectively as possible while maintaining the standard Linux memory mapper. I don't believe they touched on over provisioning, though I'm sure this concept was somewhat derived from this idea in the paper. This is a fantastic paper, offering great performance for virtualized OSes with a clear layout of the techniques used to do so. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- In previous virtualization attempts, how was trap rewriting done? 
	- Would it be a security risk to run Xen on a machine with only 2 privilege levels on the CPU? Would Xen still be able to control memory security at that point? 
	- Is BVT the primary scheduling technique for VMs still? 


  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->

	The presentation quality of this paper was excellent. It was easy to follow their implementation of the Xen system. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    5
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes, I believe it presented para-virtualization and virtualization well, and offerers a comparison between the two techniques. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>