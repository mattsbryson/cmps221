<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3158684::1551814"
        submission="19"
        title="Mnemosyne: lightweight persistent memory"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	Mnemosyne is a low level programming interface for using NVM on the memory bus. Although the system designed isn't perfect, it has much of the functionality needed to allow for the use of NVM by application programmers. It is implemented with the goals of providing easy to modify persistent data structures that have consistent states. It approaches this goal by providing programmers with persistent memory regions and persistent primitives, which is a lot simpler (and better in my opinion) than work before it. Persistent regions are stored in backing files, which given that this isn't an operating system reimplementation, isn't a bad solution to multiple applications needing to share NVM. To avoid pointers pointing to volatile memory, they create a pointer type that can only point to NVM, which I thought was a good idea, given that state can and will be reset in a system such as this one. The system is implemented as two libraries and a modified linux kernel. They choose to model this system on phase change memory (PCM) after going through several types of NVM, explaining that it is most likely to be produced in usable quantities soonest. After mentioning some of the different types of consistency, they choose to both allow for flexible consistency mechanisms as well as implementing a in place update system (logs). This is somewhat accomplished through the use of a modified compiler, which is able to convert atomic code blocks into transactions. The propose a weak solution for finding memory leaks, which I am not convinced would cover all cases - with their fallback solution being that it will be flushed to the backing store eventually. In addition, they buffer writes to PCM as much as possible for efficiency and lifespan of the memory device. The system itself handles the management of persistent memory, but doesn't state anything about security between each allocation, or if an application can request the full memory space of the system (1TB). The system uses a raw log with torn bits that is occasionally flushed to allow for truncation. The torn bit system allows for it to take linear time to determine where the clean state of the log is. Performance is good/acceptable in all cases, making Mnemosyne a good implementation of NVM access on existing systems. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- There was no security described and there was no adversarial model mentioned.
	- How would the calls to flush to NVM be similar to fsync? 
	- Has there been a system implemented to correct address multi processor concerns? They don't present an adequate solution in this paper. 


  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Good presentation, good layout.





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    4
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, it shows the use of a new way to access a new technology on systems that were never designed with it in mind. Additionally, it is a strong paper on its own, and worth reading for its content.

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>