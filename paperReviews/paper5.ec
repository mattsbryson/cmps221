<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3094615::1551814"
        submission="5"
        title="The Multikernel: A New OS Architecture for Scalable Multicore Systems"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	The premise for the Multikernel paper is relatively simple - hardware has been advancing quickly, and with new features added often to complex multicore environments, it is difficult to optimize monolithic kernels. New features can even invalidate existing optimizations, starting the process over again and causing operating systems to become more hardware dependent. The paper argues that this can be avoided with a "Multikernel" which would run semi-independently on each core. This comes from the authors' thoughts that a modern computer is closer to a high performance computer (HPC) or distributed system than the machines monolithic kernels were designed for. They show through a few small examples how messages can cost less than shared memory, and that as the number of cores increases, so does the latency. They introduce a Multikernel, which is based around CPU drivers and many user level libraries to handle messaging, memory mapping, and other functionality typically handled by a standard kernel. Unfortunately, they never make clear how involved the kernel they are running is, since they mention that it is handling the abstraction to physical memory for the system. The CPU drivers communicate using messages, and these messages allow them to agree on how memory is laid out through agreement protocols. Performance is eventually shown to be on par with existing systems in most cases. 
	
	There are a few problems with this premise and the idea of a Multikernel system. First of all, computers are not distributed systems, nor would you want to bring in the challenges of a distributed system to an operating system if you could avoid it (ie., omission failure, which typically would be detected). Agreement protocols can be expensive, and while the current setup scales for small amounts of cores, what if a GPU was connected and allowed to write to the shared memory space? Additionally, I believe many of their optimizations still could be done with an exokernel and user space libraries. Overall, this is an interesting idea, but one that would be better solved by moving more OS functionality to user space. 
	




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    1
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- Am I correct in assuming that in the event of message failure, the lack of centralized management would result in the message drop being unnoticed? 
	- Could byzantine failure result in memory being overwritten? 
	- Could features like pipelining and batching be introduced without a system like this? (Maybe with userspace servers?)


  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	The presentation was good for the most part, but some aspects were not described in enough detail. For example, I brought up in my summary that they were unclear how involved the actual kernel was, other than a brief statement mentioning that it was handling the physical memory abstraction.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Probably - this paper introduces some important optimization strategies for operating systems as well as clarifying how existing operating system handle certain operations. It brings up a legitimate research question, but it's solution might be wrong, but it is still a novel an interesting approach to investigate. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    3
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>