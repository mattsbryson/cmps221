<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3132937::1551814"
        submission="15"
        title="Light-Weight Contexts: An OS Abstraction for Safety and Performance"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	Light-weight Contexts introduces (duh) light-weight contexts, an inner process mechanism of providing memory protection, privilege seperation, snapshotting, and easy thread monitoring. Each process has a root LwC that can spawn new LwCs with fork or a creation command. When created, these LwCs have COW access to the parent memory, unless specified, as does the parent to the child. However, LwC allows child access to be limited as well, which is a great isolation mechanism in the event of an untrusted parent LwC. One important detail (and interested/good implementation choice) is that LwCs cannot be scheduled, they are completely orthogonal (best term to describe their relationship) to threads. They have an entry point for threads, and have the necessary information for the thread to resume computation where it left off. This sounds expensive, but LwC switching avoids expensive context switching by using less expensive context switching, which is a feature on new CPUs. This feature adds address space information to the TLB, protecting contexts from one another without a TLB flush. LwCs have the ability to work together by mapping parts of their memory into each other, or returning the information. Additionally, they can be "snapshotted" through the fork process, and rolled back by entering the old "state" of the LwC. I thought this was a very interesting feature - and in practice it actually significantly improves performance. When this system is actually used for isolation there is overhead, but for the added benefit, it isn't significant (added benefit being something like the Heartbleed bug). Additionally, LwCs allow for easy reference monitoring, where each privileged instruction called by the child has to be inspected and ran by the parent. This makes execution safe and transparent to the child. LwCs are an excellent idea, as they provide good memory protection and interesting features inside of processes at a low cost. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- What makes context switches expensive other than TLB flush? Rebuilding the cache? 
	- Is session isolation needed? Other than protecting from things like memory over reads like Heartbleed, does it protect web executions? (Potentially from reading each other's data?)
	- Is Heartbleed and other in process vulnerabilities the main motivation for this work? As interesting and good as it is, it seems somewhat unprompted. 
	- In BSD, do all processes exist as Jailed? 


  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->

	Presentation quality is overall OK, but is very repetitive across sections, I feel like 2 pages of this paper could easily be removed with the same information being imparted. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes, it is a very interesting way of protecting data internally in a process without a high cost. Additionally, it is an excellent example of how new hardware enables new software techniques. 




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>