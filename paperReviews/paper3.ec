<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3082149::1551814"
        submission="3"
        title="The UNIX Time-Sharing System"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary & rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary & rating
    == below. This field is required.
    -->
	
	This paper was simply the description of the Unix system and some of it's inner workings. While all familiar to us now, these concepts were new (though many were borrowed) and were combined in an easy to use and logical way. I highly doubt that anyone reading this review will not have a good understanding of Unix based systems, so descriptions of some portions of the paper will be brief. Unix was designed by a group/single programmer looking for a better way to use a "cheap" machine, and redesigned later for easier programmability (in C). It is self modified, allowing the community to update the OS and easily add features & programmers if they are lacking. The file system many of us are familiar with today (probably with a few changes) is introduced in this paper. Some core concepts introduced are i-nodes, file links, a shared file system with home directories, and of course, the concept of / (root). Additionally, init is explained, as are the features of the shell system, which is spawned with init. Permissions in files and the system used to prevent changes by certain users or programs (sometimes just the superuser) is introduced as well. The file system is buffered, but is not presented as such to the user thanks to a memory lookup before block fetch. STD in and out are introduced as well. With a paper like this it's easy to go on and on, after all it is Unix. To be brief, this paper introduced Unix, which is very very similar to the Unix based operating systems we use today. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- This paper is obviously (stated by the authors themselves) Multics driven. What did they leave out and why?
	- Why was the background process necessary? Why do many operating systems have this? 
	- Besides the sources they list (Multics & a few dead platforms), did these ideas come from anywhere else? Were most new and novel at the time? 





  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	This paper is very readable, and reads kind of like a good man page, but with more description into the inner workings of the system. 





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    5
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	It's Unix, we all use it today in one form or another, it's an obvious yes. 





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>