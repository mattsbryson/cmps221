<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3122945::1551814"
        submission="11"
        title="Live Migration of Virtual Machines"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	Live Migration of virtual machines was an excellent paper. It provided a novel solution for VM migration that's not overly complex but achieves low downtime and low transfer time. They make the case for why it is a necessary tool (pretty easy case to make) and explain where their solution falls short/where they plan to improve in the future (local disk/WAN). One thing that came to mind is that new work will need to be done in this area when NVM becomes the main memory on many of these systems. They address how VM migration is superior to app migration, both because it's more simple and network connections can be maintained. Additionally, unlike app migration, the user may not want to give the admin OS access, so it is not required for one of the transfer methods specified. The paper does an excellent job of presenting the strategies for VM transfer in the past, and for their algorithm. One issue I had was that their choose not to use a pull phase, which surprised me, but I believe I understand their reason for doing so (pull would only use hot data which is transfered, and could incur higher latency). The solution to change the destination of network traffic was simple (sending out an ARP reply/MAC spoofing). The migration process itself is well designed around the notion of writable working sets (WWS). WWS are the pages that are modified during transfer cycles. They are measured either by the OS (in one implementation) or shadow pages (in the other). The system uses their size to choose the transfer rate or if it is time to stop the system and finalize the transfer. During the final transfer, there is downtime, but in their tests this was small (a multiplayer game could be played with the downtime). The metrics were impressive, even with their adversary example.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
- What handles the "pausing" of the system? I'm guessing these are modified OSes?
- Why didn't they choose to include network page faults (pull stage)?
- VMotion probably did this first. How does this affect publishing a paper such as this one?

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->

	Excellent presentation quality, easy to follow and very interesting to read.




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    5
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes, I think this is an important papper to read, as this is a feature that is now in all hypervisors. It also covers the engineering required to move an operating system, as at first though, it is not a simple task.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>