<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3200555::1551814"
        submission="27"
        title="Operating System Profiling via Latency Analysis"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->

	Operating systems have complex interactions that are not always easy to trace, and latency holds a lot of information about their interactions. By using latency, the authors of this paper are able to deduce several errors/issues in linux and windows. The problem of using latency is the capturing of it, which can result in non portable code or induced latency. The authors of this paper attempt to increase portability by creating a profiling tool that can add the necessary execution instructions by either using the OS method of profiling, running as a driver to interpose between two functions that need to be profiled, or adding a small amount of code to generate profiling data. To reduce latency, they use techniques from statistics to decrease the amount of information that must be stored while maximizing the potential information that the profiling data has. Additionally, where data loss is small (ie., 1 percent), they tolerate the loss because not doing so would increase the overhead past their 4% target threshold. Their automated profiling tool uses differences in the analyzed data to attempt reruns of the profiler and present the user with the information necessary to deduce the issues/behavior of the profiling target. 
	
This paper presents strong results, a flexible method of profiling, and is able to achieve this with low overhead. However, their claims of portability seem weak, especially since they had to customize their method of profiling per OS. Should Linux, Windows or FreeBSD change enough (which they have) their methods may need to be redone. I don't see this as a very portable solution, but the tool set itself is flexible enough for a developer to take their methodology and tools and profile an OS. Additionally, a developer would have to already have a strong understanding of the underlying OS to correctly deduce the meaning of the data generated, so it doesn't exactly reveal as much as stated in the intro. 




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- Did I misinterpret their meaning of portable?
	- Could their methods of reducing overhead/reducing data collection result in a less correct profiling of the application?
	- Is this considered "automatic" by the profiling community? It seems to require a lot of human oversight. 
	
  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Reasonable quality, well explained though some sections could have been covered in more detail. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    4
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes, it presents profiling, which is an important research area in systems. Other profiling papers could potentially take its place however.




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    3
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>