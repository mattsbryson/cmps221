<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3082122::1551814"
        submission="1"
        title="Dynamic Storage Allocation in the Atlas Computer, Including an Automatic Use of a Backing Store"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary & rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary & rating
    == below. This field is required.
    -->

	This paper is simply the explanation of a memory management system for Atlas (as a technical report). The memory management/addressing techniques described are relatively similar to modern systems, but of course with different sizes and bit allocations. The bookkeeping operations are relatively similar to that of a modern kernel, with a lesser feature set. The executive store is a OS concept that has not stood the test of time, probably due to changes in common memory types. Additionally, the lookup and memory access systems were probably quite novel for their time, and are also similar to modern systems. The Drum Transfer learning program is very similar to a modern caching protocol, and software like it (from the limited information provided) is used in modern devices such as SSHDs. 
	
This paper is clearly excellent (limited knowledge of the state of OS research at the time). It presents novel concepts in memory management and single address stores that are still used today in some form. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- It would be useful to go over section 3 on a whiteboard to write down all the details of the paging system, since keeping track of the bit sizes and hardware different than I am typically accustomed makes it a little difficult to track internally. 
	- I'd be interested to read the Drum Transfer Learning Program paper and compare it to more recent caching protocols/see the performance on their system. 
	- Programs being able to run on different Atlas machines without code modification must have been a big deal at the time (unless this was after other OS developments). I should personally timeline papers like this to gain a greater understanding of the timeline of OS development.

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	
	This technical report does a great job of presenting a lot of useful information in a very small space. I do believe it suffers some from this, and having more informational text around the description of the system could help with paper flow and understandability of the paper. Diagrams could also be useful in presentation of the information.

Additionally, as a modern reader, it's worth scanning through and thinking to yourself which each type of old storage would be most similar to it's modern counterpart to avoid this interfering with understanding of the actual OS operations. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	The fact that so many of the principles introduced in this paper exist in modern operating systems make it an obvious choice as a paper to keep in an advanced OS class. In the case of people newer to the OS field, it also serves as a good background and prompts further exploration to better prepare them for more advanced OS papers. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>