<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3188203::1551814"
        submission="23"
        title="Microreboot - A Technique for Cheap Recovery"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	Microreboot, which would be more properly named Milli-reboot, is an interesting solution for recovering internet applications without a full reboot. Rebooting is commonly a solution in internet applications for several reasons. Rebooting refreshes the application to a clean state, meaning that any resources leaked are recovered, and any inconsistent state is flushed. The idea of microrebooting is simple - restart parts of an application (known in other work as microservices) before restarting the whole application (or application host, which in this case is two fold - a JVM on a physical machine). The paper goes deeper into the implementation and design details of such a system using eBid. The introduce a remote state to the system to allow state to be saved through reboots, and add the ability to inject faults. On fault injection, most common cases are "solved" by microreboting/microrejuvenation. Some cases are not, and as the authors point out, the time wasted during the microreboot is negligible compared to the time needed for the full reboot, and is worthwhile regardless. They also show the use of microrebooting in a distributed enivironement, where it can limit the overloading of the other machines and significantly reduce the number of lost requests (represented by their TaW metrics). Load balancers aware of microrebooting create the best uptime, and microrebooting combined with redundant clusters can help companies keep with 5 nines of availability. 
	
	
As interesting as this idea is, it is not without its issues. It is implemented in the java enterprise server, making sandboxing easy - the java virtual machine is just that, a virtual machine on top of an existing OS, meaning that it is far more contained than an application outside of it, and offers another level of reboot other than a full OS restart. With an application written in another language, I feel that it might be more difficult to implement a micro reboot system, but that it would likely still be possible. There seems to be a high barrier of entry, since it requires applications to conform to the microservice model. Also, the fact that shared state can be compromised by a microreboot is a concern, but I believe the system could be improved to attempt to make consistent shared resources before a reboot. 




  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- How useful is this concept out of java? Potentially very, but perhaps more difficult to implement and track resources? 
	- The limitations regarding shared state are a bit of an issue, given that this is common in distributed systems. 
	- The retry method with HTTP to a microrebooting server is an excellent idea, since it allows for server affinity and increases the likelihood the request will not fail. 

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	The presentation is good overall, though they should not use uncommon abbreviations as often. The amount of spaced saved is not worth it. 





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, it is a very interesting take on the rebooting process as part of high availability on an inherently buggy system (in this case, the application). It would be interesting to read a paper that does this without the assistance of java & the jvm, if such a paper exists. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    4
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>