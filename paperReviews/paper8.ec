<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3105271::1551814"
        submission="8"
        title="Application Performance and Hexibility on Exokernel Systems"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
This paper on Exokernels presents a novel implementation (and some novel concepts) on creating an exokernel system. An exokernel is a system in which privileges typically given to a kernel are given to a user level library that then acts as a kernel, but also runs the application in it's own memory space. Memory allocation is not controlled using virtual memory, but is instead handed out by the actual kernel to the user level OS library systems. These systems are typically designed for mutual or unidirectional trust, but not well designed or explained for a no trust scenario. I believe this a weakness in the paper, since they don't even claim that they have though of all the protection necessary to have this work properly. Exokernels are an interesting concept in premise, but brings up a number of issues, similar to the SPIN paper. One such issue is multiplexing disk, something the authors do a good job of solving using untrusted deterministic functions (UDFs). This essentially allows for the disk to be shared as long as there are functions for all files placed on the disk that are able to return the block mappings of the file. Additionally, because disk protections are checked when memory is accessed, not when a file read is issued, files can be prefetched regardless of permissions. I believe this leaves the system open to a potential vulnerability of a malicious library mapping files to memory and then reading into this memory without triggering kernel access control. I could be mistaken, but there does seem to be a potential security risk here. The authors prove that this system is on par, and in some cases faster than a traditional Unix system by porting Unix software and creating a library that emulates Unix. Although the performance metrics are interesting, applications seem overly expensive in a exokernel environment, so I believe performance would degrade if too many were ran on a single system. Additionally, because exokernels work well with emulation, I wonder if exokernels would be well suited for a VM like system. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- How similar are containers to an exokernel like system (without the customizability of course and different memory management)? Were exokernel concepts considered at all with their creation? 
	- Applications seem expensive to run because they bring a lot of kernel structure with them. Have tests been done to show performance when many are ran concurrently?
	- What is the current state of exokernels and what concepts remain important from this paper?  

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->

	Presentation was good overall, and the paper did a good job of covering necessary background. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    4
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes. Exokernels are still an active research topic (for hypervisors I believe). Additionally, the implementation in this paper is interesting and proposes a number of novel solutions to difficult problems.

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    4
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>