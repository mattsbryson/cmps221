<review id="3082133::1551814"
        submission="2"
        title="Introduction and Overview of the Multics System"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary & rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary & rating
    == below. This field is required.
    -->
	  The Intro to Multics paper gives a complete specification of their vision for a new operating system to allow many users to effectively share one computer. Many of the concepts presented in this paper are still used in modern operating systems today. The Multics system is designed to not be system dependent. Additionally, it is multiprogrammed, meaning that time is shared between all users and jobs, which is managed by a supervisor. This supervisor is very similar to a modern kernel, managing memory page requests, moving data in and out from secondary storage, and managing security. The creators of the system realized this would be necessary due to the increased demand for computers even from people not in computer science. They foresaw the need to have multiple processors, and the system is designed to support that. I/O backgrounding was suggested to avoid having the CPU wait on I/O tasks. Because of the flexibility of the system, any task (including the supervisor) can run on any processor, with processors having the ability to be removed and added as needed. Communication between them is done asynchronously using mailboxes, which was a revolutionary thought at the time. Additionally, there is a virtual address space with segments representing data blocks, which has the advantage of allowing data to be pulled in at will, entry points specified, etc. Segments have descriptor bits to show how they should be treated, ie. if they should be executed or read or are only data. Additionally, they are called symbolically, and dynamically bound at run-time. Additionally, there was a file system with ACL like configurability. Multics was an OS ahead of its time, laying down the ground work for the systems we enjoy today. 
  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
	3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	  - It is incredible that they were able to predict multiprocessors so far ahead of the need to create them, and their scheme to allow for multiple processors to work together is amazingly close to how systems work with multiple processors today. 
	  - So many concepts are still used today, are there any that have fully changed? 
	  - Would the multics system allow for hot swapping of components? Or would that require a system restart? How did such an activity occur for multics? 

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	  Although the paper could have been a little better with organization, the paper is well presented, and it is relatively easy for the reader to understand and learn about the Multics system. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
	  4
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	  In an OS class, this question barley applies. Multics is such an important paper and covers so many operating system principles. It was far ahead of its time, and the concepts presented are still used today. 
  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
	  5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>

<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->