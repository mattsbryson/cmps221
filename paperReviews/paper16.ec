<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3141269::1551814"
        submission="16"
        title="Sharing and Protection in a Single-Address-Space Operating System"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	"Sharing and Protection in a Single-Address-Space Operating System" or Opal, is a paper describing the design and partial implementation of a single address space operating systems. The authors believe due to the (then current) creation of wide address processors (64 bit), that separate virtual address spaces ("small addressing on a big address space") was no longer required, and that protection domains should be decoupled from address spaces (as with any good systems paper, the word decoupled is used frequently). The authors argue that single address spaces make sharing easier between applications, increase performance, all without a loss of security. The paper goes into the tradeoffs, between the Opal system and a traditional multi-address space approach, and at least for applications requiring better sharing and performance, a single address space seems a better option. Several new systems terms are used in this paper because most of the terms used to describe systems components in Unix don't apply in Opal. All data is stored in segments (continuous virtual pages), which can store code or data. Threads run inside a protection domain, which is as close as Opal comes to the concept of a Unix process. Protection domains can be nested. Data and code is mapped to a protection domain using attach and detach mechanisms, with capabilities determining if the protection domain is allowed to map the segment. Attaching and detaching (along with Resource Groups & Resource Objects to organize and separate them) allow Opal to know when data is not mapped, and can be freed from memory. Data is all mapped in a backing store, so even when not in physical memory, the segments can be brought back. Additionally, segments can be published so in the event of a address fault, the correct segment can attempt to be attached. Domain communication is done using portals, which use shared memory and carefully controlled entry points to allow for cross domain communication. An advantage of running code on this system is that any code referenced is only loaded once, instead of into every address space, and this one copy can be shared. Sharing and globalization of resources are some of the main points for this type of system implementation, but it is not without it's drawbacks.
	
One big drawback in Opal is that due to the global address space, copied data objects have to have their pointers remapped, making copy on write difficult. The authors solve this using a clever method of referencing back to the original object until the data is overwritten. Additionally, Opal was implemented on top of Mach, making performance testing more difficult because of the Mach kernel interaction - the authors themselves state that better performance could have been gained if they wrote their own kernel. Additionally, their reasoning for writing on Mach (allowing for other applications to run) was weak considering the strong stance they take on moving away from Unix ideals and their format. Additionally, they point out a potential issue is that segments cannot be expanded, and will not be contiguous unless created at the same time. Given the other mechanisms of Opal, I do not believe this is an issue.

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- They use Mach to handle persistance, did they have a plan to handle it themselves? Or would an actual Opal implementation just include a backing store for each segment? 
	- Did they actually present a solution for dangling references? It seems likely to happen. 
	- In an actual Opal implementation, would all domains need kernel threads? 
	- Were performance metrics ever created for the Boeing database system?
	
  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	The paper is broken up in a way that at first makes it easier to understand the concepts presented, but then requires you to skip back as topics are retouched upon in different order and elaborated upon. Also, this feels like several papers stuck together, and could (and should) be trimmed down to impart the core concept. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, a single address space operating system is an interesting concept that has relevance for future work. Additionally, this is another important OS design to consider when building or thinking about operating systems.

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>