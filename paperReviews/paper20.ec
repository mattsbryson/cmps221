<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3158689::1551814"
        submission="20"
        title="Software Persistent Memory"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	SoftPM is a very interesting implementation of orthogonal persistence for programs wanting to persist a section of their memory. Memory is tracked using containers, which only require the root pointer to a data structure to persist the entire structure. Cross structure references are handled well by SoftPM, which simply uses the pointer to persist a new structure in a container. SoftPM is made up of two component parts, one of which is LIMA, which handles memory management - one of the more interesting actions being page management to track what data needs to be persisted on change. This is achieved by writing the pages as read only, and when they're faulted, marking them as dirty. LIMA also handles the creation of persistence points, which block all threads while it is written to persistent media. LIMA is responsible for reading back in containers as well, and swizzling the pointers to ensure pointer consistency. This is not described in enough detail in my opinion. The second main part of SoftPM is the SID, which persists these pages in different page chunks (which may be optimized in size for the type of persistent media that they will be stored to). SID does the actual writing of the persistence points, which involve blocking all threads while the data is written. I feel that this is a poor strategy, but other strategies would have their own drawbacks. SID is well optimized for flash and disk, using over-provisioning to allow for sequential writes on disk and matching flash erase size on flash. SoftPM uses an allocation pointer and static hints in compiled C to track the allocation, creation, and deallocation of pointers. SoftPM performs well in a variety of cases, and is consistent in all listed cases. Despite a few drawbacks, this is very interesting work.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- Blocking sucks, why choose to block when writing a consistency point? Seems like the easy way out. 
	- How are they actually determining the new addresses for the reloaded containers to Swizzler? I feel like they didn't cover this well. 
	- What is a transitive memory closure? 

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	It could use work, there should have been more detail in the design description, and perhaps less performance evolution (and more consistency testing).

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, despite the drawbacks the idea is interesting, even if some details of the implementation could be changed or better explained. 
	
  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    4
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>