
<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3132914::1551814"
        submission="13"
        title="Arrakis: The Operating System is the Control Plane"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
	The motivation for Arrakis seems to lie with 2 hardware trends and one goal - to reduce kernel overhead when managing network and storage devices. These trends are the increased speed of both networking and disk, and the increasing amount of hardware support for virtualization. This system uses a combination of virtualization and userspace techniques to successfully move management of these resources out of the kernel and significantly increase performance. This need is shown by their worse case analysis of both devices, though without the follow up performance comparison at the end, I would have thought that they went too far with their assumptions. In Arrakis, security is handled by the virtualization features of the devices, which successfully multiplexes them using the application library and (for now) the server system that emulates the virtualization features the authors would eventually like to see in network and storage hardware. Each application is given a virtual VNIC or VSIC, depending on what device is being emulated to the application, with it's own queues and mappings (in the case of a file system). Luckily, since the application is essentially given direct control over the device, files can be shared by exporting them to the kernel's file system. Both use doorbell mechanisms (which both seemed to support and not support interrupts at the same time - the paper was not clear) to signal to an application a task had been completed with the I/O device. This is all implemented on top of the libOS in Barellfish. Performance is shown to be good over a number of real world metrics when compared to Linux, and their non posix (data structure in storage) system outperforms their POSIX compliant system when applicable. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- Did they ever get the hardware they hoped to come in the future? Are hardware level protections now supported?
	- Could Barrelfish have given them a performance advantage since it's an exokernel? Does it normally outperform linux with set tasks? 
	- Could this be implemented on top of VMs? I believe intel chips have enough security rings potentially... 
	- Did they support interrupts? The paper is unclear but hardware interrupts seem too obvious to ignore. 
	

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->

	The presentation is good overall, with some drawbacks. Reading is clear and the explanation of the system is good - but there are some details left out at times (the abstract is lacking), or repeated at the wrong times. Additionally, a clear distinction should have been drawn between network and disk, instead of haphazardly jumping back and forth between them.


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    2
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->

	Yes, it shows applications of certain technologies (ie. virtualization hardware) aren't limited to the systems they are designed for. Additionally, it is a well thought out paper with an interesting concept. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    4
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>