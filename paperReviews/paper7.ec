<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3105268::1551814"
        submission="7"
        title="Extensibility safety and performance in the SPIN operating system"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
Non extensible operating systems can be optimized for a single use case, or generally optimized but not give good performance in any particular use case. Lack of interfaces & poor implementation can range from hampering performance to stopping an application from running entirely. To allow for better functionality and performance for applications, the authors introduce SPIN, which is an extensible operating system. When I was first approaching this paper, I assumed there would be some good work, since existing operating systems offer kernel extensions, but I didn't expect the implementation that they proposed, which baring security concerns (which may be unfounded), is very clever. SPIN is extensible and safe through kernel code checking and language checking in Modula-3. Until this paper I had never thought of using programming language principles to ensure safety, but doing so for an extensible kernel is a great idea, and reduces the attack surface considerably considering that the language seeks to keep the programmer out of memory outside it's virtual address space and doesn't allow them to keep access to deallocated memory (honestly reminds me a bit of Java, but more low level). The memory, thread, and scheduling management systems are incredible, only offering the lowest level services so that extensions can customize for each program. Additionally, they all offer excellent performance. Also, the system is relatively safe (from crashing at least) since if an extension fails only the extensions and applications that rely on it will fail. Additionally, when installing extensions, their compatibility with existing extensions is checked. I believe there may be a way to exploit this system to gain kernel access, but I'm not sure what it would be, other than to write a trusted extension - but this issue/design choice is mentioned by the paper. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    2
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- What does safe by assertion mean? How is this defined if the extension is written in C instead of Modula-3. 
	- How does performance change when you have a lot of extensions running? 
	- Is there a way to gain kernel access except through trusted extension? How tight is their security in retrospect? Do any current systems use programming languages as protection (other than sandbox like languages like java)?

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Ordering of sections could be improved (ie. the justification for Modula3), and some background concepts, especially from programming languages could be better explained. 


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    3
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Definitely, this paper has a very interesting approach to setting up a extensible operating system and the read definitely got me to change the way I think about operating systems to some degree. Even though this paper isn't technically a microkernel, I think the approach they took is a great way to userspace typically kernel operations while leaving core functionality in the kernel.  

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>