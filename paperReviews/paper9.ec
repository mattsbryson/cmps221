<!--
  == For your convenience, this form can be processed by EasyChair
  == automatically. You can fill out this form offline and then upload it
  == to EasyChair. Several review forms can be uploaded simultaneously.
  == You can modify your reviews as many times as you want.
  == 
  == When filling out the review form please mind the following rules:
  == 
  == (1) Blocks such as this are comments. EasyChair will ignore them.
  ==     Do not write any text into these blocks as it will be ignored.
  ==     You can add comments to the review form or remove them.
  == (2) Write only into the tags where instructed. Do not modify any
  ==     tags and attributes, or the review will become unusable and will
  ==     be rejected by EasyChair.
  -->
<review id="3105272::1551814"
        submission="9"
        title="EbbRT: A Framework for Building Per-Application Library Operating Systems"
        authors="Ethan Miller"
        pc_member="Matthew Bryson">
<field id="414912" name="Overall summary and rating">
  <!--
    == Please provide a detailed summary, including a justification for
    == your rating. Both the rating and the summary are required. You may
    == not cut and paste text from the paper (or elsewhere) for your
    == summary!
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Overall summary and rating
    == below. This field is required.
    -->
EbbRT is essentially a cloud exokernel with changes that makes it a better fit for that environment. EbbRT also seeks to be easier to develop library operating systems on, which it does by making all default library operating systems easily extensible or replaceable. EbbRT is implemented as both a bootable application framework (basically a custom exokernel) and as a system that can run inside a linux process. Both are assumed to be running on VMs, since they allow for the use of general purpose libraries from a standard monokernel OS but having a EbbRT runtime that exists on a monolithic OS share the runtime. Performance is worse on the process version of EbbRT, but I think it is a useful feature that speaks to the easy of development of the platform. EbbRT is event driven, which allows for applications to be ran when needed. Memory management is also extensible, and existing in the form of 4 LibOSes working in conjuncture. Because event driven programming breaks up the traditional control flow developers are accustomed to, EbbRT uses futures, which are a data type that will eventually be filled. This is an excellent idea given the layout of the OS. The network stack is also novel, giving Iobuf primitives instead of traditional sockets. Apps are given direct control over this, since they know best how to handle the IO buffer. Performance across the system is on par or significantly better. 

  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 3 Excellent
    == 2 Very good
    == 1 Good
    == 0 OK
    == -1 Not so great
    == -2 Ugh!
    -->
    3
  </score>
</field>
<field id="417009" name="Three questions / comments about the paper">
  <!--
    == Please enter three questions or comments you have about the paper.
    == You should plan to use these in our in-class discussion.
    -->
  <text required="required">
  <!--
    == Enter the text for the field
    ==   Three questions / comments about the paper
    == below. This field is required.
    -->
	- How do the separate EbbRT instances communicate? They mention they function like a distributed system, but not how. 
	- Has this paper been successful? I think it's an excellent idea, and with companies trying to save money in the cloud, I'd expect them to feel the same. 
	- Is event driven execution the only method of scheduling? 

  </text>
</field>
<field id="417010" name="Presentation quality">
  <!--
    == How would you rate the presentation of the paper? This includes
    == organization, writing, visuals (graphs / figures), and overall
    == understandability. You may include comments if you like to explain
    == your rating, but this is optional.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Presentation quality
    == below. This field is optional.
    -->
	Presentation quality was good, paper was a bit dense but it can't be faulted for that. 





  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Excellent
    == 4 Very good
    == 3 Good
    == 2 Fair
    == 1 Poor
    -->
    4
  </score>
</field>
<field id="417008" name="Should we read this paper the next time graduate operating systems is offered?">
  <!--
    == Overall, do you think we should read this paper the next time we
    == teach graduate operating systems? You may (optionally) include
    == comments to explain your rating.
    -->
  <text optional="optional">
  <!--
    == Enter the text for the field
    ==   Should we read this paper the next time graduate operating systems is offered?
    == below. This field is optional.
    -->
	Yes, this is a very interesting implementation of a exokernel, and it's interesting to see these concepts implemented in light of new developments (ie. cloud).


  </text>
  <score>
  <!--
    == Select your choice from the options below and write its number below,
    == before the </score> tag.
    ==
    == 5 Definitely yes
    == 4 Yes
    == 3 Probably
    == 2 No
    == 1 Definitely no
    -->
    5
  </score>
</field>
<reviewer>
  <!--
    == If the review was written by (or with the help from) a subreviewer
    == different from the PC member in charge, add information about
    == the subreviewer below. Write subreviewer's first name, last name
    == and email between the tags below.
    -->
  <first_name>  </first_name>
  <last_name>   </last_name>
  <email>  </email>
</reviewer>
</review>